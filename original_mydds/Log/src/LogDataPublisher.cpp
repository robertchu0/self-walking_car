#define VORTEX_COMMUNITY
/*
 *                         OpenSplice DDS
 *
 *   This software and documentation are Copyright 2006 to 2013 PrismTech
 *   Limited and its licensees. All rights reserved. See file:
 *
 *                     $OSPL_HOME/LICENSE
 *
 *   for full copyright notice and license terms.
 *
 */

/************************************************************************
 * LOGICAL_NAME:    LogDataPublisher.cpp
 * FUNCTION:        OpenSplice Tutorial example code.
 * MODULE:          Tutorial for the C++ programming language.
 * DATE             September 2010.
 ************************************************************************
 *
 * This file contains the implementation for the 'LogDataPublisher' executable.
 *
 ***/
#include <string>
#include <sstream>
#include <iostream>
#include "DDSEntityManager.h"
#include "ccpp_LogData.h"
 #include "higherfunc.h"
#if defined VORTEX_COMMUNITY
  #include "os.h"
#else
  #include "vortex_os.h"
#endif


#include "example_main.h"

using namespace DDS;
using namespace LogData;

/* entry point exported and demangled so symbol can be found in shared library */
extern "C"
{
  OS_API_EXPORT
  int LogDataPublisher(int argc, char *argv[]);
}

int LogDataPublisher(int argc, char *argv[])
{

  char sensor1[]="sensor1_data";
  char topic_send[] = "Send";
  char topic_response[] = "Response";
  send_message(sensor1, topic_send);
  string responseMessage = get_information(topic_response);
  cout << "The response message is " << responseMessage << endl;

  return 0;
}

int OSPL_MAIN (int argc, char *argv[])
{
  return LogDataPublisher (argc, argv);
}
