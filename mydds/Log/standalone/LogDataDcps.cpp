//******************************************************************
// 
//  Generated by IDL to C++ Translator
//  
//  File name: LogDataDcps.cpp
//  Source: LogDataDcps.idl
//  Generated: Wed May  3 17:50:33 2017
//  OpenSplice V6.4.140407OSS
//  
//******************************************************************

#include "LogDataDcps.h"

#if DDS_USE_EXPLICIT_TEMPLATES
template class DDS_DCPSUVLSeq < LogData::Msg, struct MsgSeq_uniq_>;
#endif

const char * LogData::MsgTypeSupportInterface::_local_id = "IDL:LogData/MsgTypeSupportInterface:1.0";

LogData::MsgTypeSupportInterface_ptr LogData::MsgTypeSupportInterface::_duplicate (LogData::MsgTypeSupportInterface_ptr p)
{
   if (p) p->m_count++;
   return p;
}

DDS::Boolean LogData::MsgTypeSupportInterface::_local_is_a (const char * _id)
{
   if (strcmp (_id, LogData::MsgTypeSupportInterface::_local_id) == 0)
   {
      return true;
   }

   typedef DDS::TypeSupport NestedBase_1;

   if (NestedBase_1::_local_is_a (_id))
   {
      return true;
   }

   return false;
}

LogData::MsgTypeSupportInterface_ptr LogData::MsgTypeSupportInterface::_narrow (DDS::Object_ptr p)
{
   LogData::MsgTypeSupportInterface_ptr result = NULL;
   if (p && p->_is_a (LogData::MsgTypeSupportInterface::_local_id))
   {
      result = dynamic_cast < LogData::MsgTypeSupportInterface_ptr> (p);
      if (result) result->m_count++;
   }
   return result;
}

LogData::MsgTypeSupportInterface_ptr LogData::MsgTypeSupportInterface::_unchecked_narrow (DDS::Object_ptr p)
{
   LogData::MsgTypeSupportInterface_ptr result;
   result = dynamic_cast < LogData::MsgTypeSupportInterface_ptr> (p);
   if (result) result->m_count++;
   return result;
}

const char * LogData::MsgDataWriter::_local_id = "IDL:LogData/MsgDataWriter:1.0";

LogData::MsgDataWriter_ptr LogData::MsgDataWriter::_duplicate (LogData::MsgDataWriter_ptr p)
{
   if (p) p->m_count++;
   return p;
}

DDS::Boolean LogData::MsgDataWriter::_local_is_a (const char * _id)
{
   if (strcmp (_id, LogData::MsgDataWriter::_local_id) == 0)
   {
      return true;
   }

   typedef DDS::DataWriter NestedBase_1;

   if (NestedBase_1::_local_is_a (_id))
   {
      return true;
   }

   return false;
}

LogData::MsgDataWriter_ptr LogData::MsgDataWriter::_narrow (DDS::Object_ptr p)
{
   LogData::MsgDataWriter_ptr result = NULL;
   if (p && p->_is_a (LogData::MsgDataWriter::_local_id))
   {
      result = dynamic_cast < LogData::MsgDataWriter_ptr> (p);
      if (result) result->m_count++;
   }
   return result;
}

LogData::MsgDataWriter_ptr LogData::MsgDataWriter::_unchecked_narrow (DDS::Object_ptr p)
{
   LogData::MsgDataWriter_ptr result;
   result = dynamic_cast < LogData::MsgDataWriter_ptr> (p);
   if (result) result->m_count++;
   return result;
}

const char * LogData::MsgDataReader::_local_id = "IDL:LogData/MsgDataReader:1.0";

LogData::MsgDataReader_ptr LogData::MsgDataReader::_duplicate (LogData::MsgDataReader_ptr p)
{
   if (p) p->m_count++;
   return p;
}

DDS::Boolean LogData::MsgDataReader::_local_is_a (const char * _id)
{
   if (strcmp (_id, LogData::MsgDataReader::_local_id) == 0)
   {
      return true;
   }

   typedef DDS::DataReader NestedBase_1;

   if (NestedBase_1::_local_is_a (_id))
   {
      return true;
   }

   return false;
}

LogData::MsgDataReader_ptr LogData::MsgDataReader::_narrow (DDS::Object_ptr p)
{
   LogData::MsgDataReader_ptr result = NULL;
   if (p && p->_is_a (LogData::MsgDataReader::_local_id))
   {
      result = dynamic_cast < LogData::MsgDataReader_ptr> (p);
      if (result) result->m_count++;
   }
   return result;
}

LogData::MsgDataReader_ptr LogData::MsgDataReader::_unchecked_narrow (DDS::Object_ptr p)
{
   LogData::MsgDataReader_ptr result;
   result = dynamic_cast < LogData::MsgDataReader_ptr> (p);
   if (result) result->m_count++;
   return result;
}

const char * LogData::MsgDataReaderView::_local_id = "IDL:LogData/MsgDataReaderView:1.0";

LogData::MsgDataReaderView_ptr LogData::MsgDataReaderView::_duplicate (LogData::MsgDataReaderView_ptr p)
{
   if (p) p->m_count++;
   return p;
}

DDS::Boolean LogData::MsgDataReaderView::_local_is_a (const char * _id)
{
   if (strcmp (_id, LogData::MsgDataReaderView::_local_id) == 0)
   {
      return true;
   }

   typedef DDS::DataReaderView NestedBase_1;

   if (NestedBase_1::_local_is_a (_id))
   {
      return true;
   }

   return false;
}

LogData::MsgDataReaderView_ptr LogData::MsgDataReaderView::_narrow (DDS::Object_ptr p)
{
   LogData::MsgDataReaderView_ptr result = NULL;
   if (p && p->_is_a (LogData::MsgDataReaderView::_local_id))
   {
      result = dynamic_cast < LogData::MsgDataReaderView_ptr> (p);
      if (result) result->m_count++;
   }
   return result;
}

LogData::MsgDataReaderView_ptr LogData::MsgDataReaderView::_unchecked_narrow (DDS::Object_ptr p)
{
   LogData::MsgDataReaderView_ptr result;
   result = dynamic_cast < LogData::MsgDataReaderView_ptr> (p);
   if (result) result->m_count++;
   return result;
}



