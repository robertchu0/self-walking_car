#ifndef LOGDATASPLTYPES_H
#define LOGDATASPLTYPES_H

#include "ccpp_LogData.h"

#include <c_base.h>
#include <c_misc.h>
#include <c_sync.h>
#include <c_collection.h>
#include <c_field.h>

extern c_metaObject __LogData_LogData__load (c_base base);

extern c_metaObject __LogData_Msg__load (c_base base);
extern const char * __LogData_Msg__keys (void);
extern const char * __LogData_Msg__name (void);
struct _LogData_Msg ;
extern  c_bool __LogData_Msg__copyIn(c_base base, struct LogData::Msg *from, struct _LogData_Msg *to);
extern  void __LogData_Msg__copyOut(void *_from, void *_to);
struct _LogData_Msg {
    c_long userID;
    c_string message;
};

#endif
