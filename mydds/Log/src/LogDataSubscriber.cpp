#define VORTEX_COMMUNITY

/*
 *                         OpenSplice DDS
 *
 *   This software and documentation are Copyright 2006 to 2013 PrismTech
 *   Limited and its licensees. All rights reserved. See file:
 *
 *                     $OSPL_HOME/LICENSE
 *
 *   for full copyright notice and license terms.
 *
 */

/************************************************************************
 * LOGICAL_NAME:    LogDataSubscriber.cpp
 * FUNCTION:        OpenSplice Log example code.
 * MODULE:          Log for the C++ programming language.
 * DATE             September 2010.
 ************************************************************************
 *
 * This file contains the implementation for the 'LogDataSubscriber' executable.
 *
 ***/
#include <string>
#include <sstream>
#include <iostream>
#include "DDSEntityManager.h"
#include "ccpp_LogData.h"
#include "higherfunc.h"
#include "hiredis.h"
#if defined VORTEX_COMMUNITY
  #include "os.h"
#else
  #include "vortex_os.h"
#endif


#include "example_main.h"

using namespace DDS;
using namespace LogData;

/* entry point exported and demangled so symbol can be found in shared library */
extern "C"
{
  OS_API_EXPORT
  int LogDataSubscriber(int argc, char *argv[]);
}

int LogDataSubscriber(int argc, char *argv[])
{

char topic_send[] = "Send";
string message_from_send_topic; 
message_from_send_topic=get_information(topic_send);
 cout<<"the receive message is "<<message_from_send_topic<<endl;
string set = "set temperature "+message_from_send_topic;
cout << "set " << set << endl;
char charset[set.size()];
strcpy(charset,set.c_str());
  redisContext *redis = (redisContext *) redisConnect("127.0.0.1", 6379);
    if ((redis == NULL) || (redis->err))
    {
        cout << "Fail to connect..." << endl;
        return -1;
    }
    redisReply *r = (redisReply *) redisCommand(redis,charset);
    r = (redisReply *) redisCommand(redis, "get temperature");
//    cout << "The value of key 'a' is " << endl;
    cout << r->str << endl;
    freeReplyObject(r);
    redisFree(redis);
 // char topic_response[] = "Response";

//  char empty_message[] = "empty";
//  char reply_message[]= "got you";     
//while(1)
//{

//if(message_from_send_topic=="")
//{
//send_message(empty_message, topic_response);
//cout<<"Due to empyty message, Sub ends"<<endl;
//}

//send_message(reply_message, topic_response);
return 0;
}
int OSPL_MAIN (int argc, char *argv[])
{
  return LogDataSubscriber (argc, argv);
}
